# sa_traffic_analysis
SA Traffic Analysis repository for people to show case data science and analytics skills

## Purpose & Objective:
This repo has been put together to allow a take home challenge to be carried out involving retrieval, analysis and show casing of traffic crash data regarding crashes in South Australia in 2018 & 2019.

There are two overarching objectives for this take home challenge:

1. That you can use basic git commands to retrieve and download data to a local machine where you can perform your analysis
2. That you can perform some form of exploratory data analysis using Python (3.X) to tell us a story using the crash dataset in this repo. What story does the data tell? What do you think is the most impactful part of this data set? If this dataset was to be used in a data science project, what do you think could be done with it? (Note: This is not the aim of this take home challenge, so please do not make fancy ML models to demonstrate what you can do in Python with ML/DS)

## Important note:
It is the intention of this take home challenge to be something you are able to pickup and work on for a few hours, nothing more. Please do not spend more than a few hours as the interview will use ~10 minutes to assess what you have done! It is also the aim of this challenge that you can a) Learn something from it and b) Ideally have fun doing :)

## Deliverables:

EITHER:
- Preferably: A Jupyter/Google colab/Other form of notebook with your code, visualisations and findings (hint: please ensure code is _readable_ and well documented!)
- OR:A small Python project that is easy to understand your code, visualisations and work in

### Please email your deliverable(s) to: dan.bridgman@transport.nsw.gov.au

## Instructions:
1. Please carry out a git clone to retrieve the necessary data files and associated meta-data for this project by carrying out:

```bash
git clone https://github.com/datadanb/sa_traffic_analysis.git
```
2. Using the IDE or Notebook interface of your choice carry out your EDA based on the objectives above. Note that the meta-data file will be very useful for understanding more about the data
3. Send your deliverable(s) to: dan.bridgman@transport.nsw.gov.au
